﻿using System;
using System.IO;
using System.IO.Compression;
using System.Text;

public static class ZipHelper
{
	public static byte[] Decompress(byte[] input)
	{
		byte[] result;
		using (MemoryStream memoryStream = new MemoryStream(input))
		{
			using (MemoryStream memoryStream2 = new MemoryStream())
			{
				using (DeflateStream deflateStream = new DeflateStream(memoryStream, CompressionMode.Decompress))
				{
					deflateStream.CopyTo(memoryStream2);
				}
				result = memoryStream2.ToArray();
			}
		}
		return result;
	}

	public static string Unzip(string input)
	{
		if (string.IsNullOrEmpty(input))
		{
			return input;
		}
		string result;
		try
		{
			byte[] bytes = ZipHelper.Decompress(Convert.FromBase64String(input));
			result = Encoding.UTF8.GetString(bytes);
		}
		catch (Exception)
		{
			result = input;
		}
		return result;
	}
}

public class Program
{
	public static void Main()
	{
        Console.Write("Enter string: ");
        string enctext = Console.ReadLine();
		string text = ZipHelper.Unzip(enctext);
		Console.WriteLine("\nEncoded text \"" + enctext + "\"");
        Console.WriteLine("\nDecoded text \"" + text + "\"");
	}
}