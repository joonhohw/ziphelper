# ZipHelper

This program can be used to decode encoded strings within compromised versions of `SolarWinds.Orion.Core.BusinessLayer.dll` during the course of reverse engineering or investigation.